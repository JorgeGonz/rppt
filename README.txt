Welcome to the RPA Post-Processing Tool (RPPT) repository. This tool is intended
to be used to process the data from the RPA operated at the UPMPlasmaLab.

- Obtaining the code
The code can be obtained via Git from J. Gonzalez personal repository:

  git clone https://gitlab.com/JorgeGonz/rppt

- Requirements
The tool uses Python 3.

- Execution
The main execution in Unix systems is:

  python main.py <config_file>

<config_file> is any text file with a set of sections and variables that define
the execution of the program. The defult one is named 'config.ini'

The file names of the RPA measurements need to follow a constrained structure:

  RPA_Z???_R_X???_Y???.txt

in which ? are substituted by numbers or the symbol minus (-). Z, X and Y 
represent the position of the measurement.

Once completed, the post-processed data will be stored in a new folder. The two 
peaks of the distribution function are stored in the file peaks.txt. All the 
filtered distributions functions are stored in a folder named 
'distributionFunction' in which the name of the file contains the position of
measurement.

- Config file
The config file is divided in sections, delimited by []. These sections contain a
series of variables that define relevant parameters for the execution of the
code.

The sections, options and valules are case-sensitive.

The sections and their options are explained next:

  - input: defines the location of the input data and how to read it.
    - inputFolder: path to the folder containing all RPA files

  - experiment: data from the experimental configuration. These are moslty used
                to organize the post-processed data in folders. The main folder
                is composed by 'date_label'. If no 'label' is provided, then
                'date' is only used. If no date is provided, the current date 
                is used as a folder name. After that, the data are organized in
                subfolders by gas flow, current, and acceleration voltage.
    - Vacc: acceleration voltage in V
    - Ib: truster current in mA
    - flow: gas flow in sccm
    - gas: type of gas (Argon or Xenon)
    - date: date of the experiment
    - label: text label for the data

  - Savitzky-Golay: options for the SG filtering function.
    - order: polynomial order.
    - frameSize: size of the data frame for filtering.
  - output
    - plotFigures: save the plotting of the peaks, mean velocity and temperature
                   to check the output.
    - plotDistFunc: plot the distribution functions

