#!/usr/bin/env python
import os
import sys
import configparser
import scipy.signal as signal
import scipy.integrate as integrate
import numpy as np
import matplotlib.pyplot as plt
from datetime import date
from scipy.constants import e, physical_constants
from math import sqrt

# Class to store configuration read from the input file
class Config:
    inputFolder = ''
    outputFolder = ''
    plotFigures = False
    plotDistFunc = False
    frameSize = 0.0
    polyOrder = 0
    chargeMassRatio = 0.0
    def __init__(self, inputFile):
        sep = os.path.sep

        inputConfig = configparser.ConfigParser()
        inputConfig.read(inputFile)

        # Get input file
        self.inputFolder  = inputConfig.get('input','inputFolder')

        mass = 0.0
        gasName = inputConfig.get('experiment', 'gas')
        if   gasName == 'Argon':
            massU = 39.948

        elif gasName == 'Xenon':
            massU = 131.293

        else:
            print('Error: gas ' + gasName + ' not identified')
            exit()

        mass = massU * physical_constants['atomic mass constant'][0]

        self.chargeMassRatio = e / mass

        # Create output directory
        if inputConfig.has_option('experiment', 'date'):
            dateExperiment = inputConfig.get('experiment', 'date')
            if inputConfig.has_option('experiment', 'label'):
                self.outputFolder = dateExperiment + '_' + inputConfig.get('experiment', 'label')
            else:
                self.outputFolder = dateExperiment

        else:
            self.outputFolder = date.today().strftime('%Y.%m.%d')

        flow = inputConfig.get('experiment','flow')
        Ib   = inputConfig.get('experiment','Ib')
        Vacc = inputConfig.get('experiment','Vacc')

        self.outputFolder += sep + flow + 'sccm' + sep + Ib + 'mA' + sep + Vacc + 'V' + sep

        # Get frame size
        self.frameSize  = inputConfig.getfloat('Savitzky-Golay','frameSize')
        if self.frameSize <= 0.0:
            print('frameSize not specified.\nStopping execution')
            exit()

        # Get the polynomial order
        self.polyOrder = inputConfig.getint('Savitzky-Golay','order')
        if self.polyOrder < 2:
            self.polyOrder = 2
            print("Setting polynomial order to 2")

        # Get if the figures need to be ploted
        self.plotFigures = inputConfig.getboolean('output','plotFigures')

        # Get if the distributions functions are plotted
        self.plotDistFunc = inputConfig.getboolean('output','plotDistFunc')

# Class to store the data of a point
class dataPoint:
    Z = 0.0
    X = 0.0
    Y = 0.0
    V  = np.array([])
    f  = np.array([])
    df = np.array([])
    I  = np.array([])
    indexPeak1 = 0
    indexPeak2 = 0
    indexSep = 0
    u1 = 0.0
    T1 = 0.0
    u2 = 0.0
    T2 = 0.0
    def __init__(self, Z, X, Y):
        self.Z = Z
        self.X = X
        self.Y = Y

# Class to store the output from the SG filter
class dataSG:
    V   = np.array([])
    f   = np.array([])
    df  = np.array([])
    ddf = np.array([])
    def __init__(self, V, f, df, ddf):
        self.V   = V
        self.f   = f
        self.df  = df
        self.ddf = ddf

# Class to store the raw data read from a file
class rawData:
    V = 0.0
    I = 0.0
    def __init__(self, voltage, current):
        self.V = voltage
        self.I = current

# Main program
def main():
    # Inits the list to store the SG data
    dataPost = []

    # Get the input file from the argument list and inits the configuration
    config = Config(str(sys.argv[1]))

    # Get the data for all files in the inputFolder
    dataPost = readFolder(config)
    
    # Write data in ASCII format
    writeASCII(config, dataPost)

    # Plot figures
    plotFigures(config, dataPost)

    # Plot Distribution Functions
    plotDistFunc(config, dataPost)

def readFolder(config):

    # Post processing data
    dataPost = []
    
    # Get the list of files in the folder
    fileList = os.listdir(config.inputFolder)

    for item in fileList:
        # Obtain the coordinates from the filename
        nameSplit = os.path.splitext(item)[0].split('_')
        if not nameSplit[1][0] == 'Z' or \
           not nameSplit[3][0] == 'X' or \
           not nameSplit[4][0] == 'Y':
            print('Error in filename format: ' + item)
            exit()
        
        dataPost.append(dataPoint(int(nameSplit[1][1:]), \
                                  int(nameSplit[3][1:]), \
                                  int(nameSplit[4][1:])))

        index = len(dataPost) - 1

        # Get the raw data from the file
        data = readRPAFile(config.inputFolder + item)

        # Calculate window length
        windowLength = int(config.frameSize * len(data))

        # Correct for even windowLenght
        if windowLength % 2 == 0:
            windowLength += 1

        # Calculate the grid spacing
        dV = data[1].V - data[0].V

        # Apply SG filter
        f   = signal.savgol_filter([d.I for d in data], window_length = windowLength, polyorder = config.polyOrder, delta = dV, deriv = 0)
        df  = signal.savgol_filter([d.I for d in data], window_length = windowLength, polyorder = config.polyOrder, delta = dV, deriv = 1)
        ddf = signal.savgol_filter([d.I for d in data], window_length = windowLength, polyorder = config.polyOrder, delta = dV, deriv = 2)

        # Locate index of maximum current
        fMaxLoc = np.where(f == np.amax(f))[0][0]
        # Locate index from which -df is positive
        dfZero = np.where(df[fMaxLoc:] < 0)[0][0] + fMaxLoc

        # Assign data from first valid point to end
        dataPointSG = dataSG([d.V for d in data[dfZero:]], f[dfZero:], df[dfZero:], ddf[dfZero:])

        # Shifting potential axis so first element is V = 0
        V0 = dataPointSG.V[0]
        for i in range(len(dataPointSG.V[:])):
            dataPointSG.V[i] -= V0

        # Copy data from SG to the data point
        dataPost[index].V  =   dataPointSG.V
        dataPost[index].f  = - dataPointSG.df  * config.chargeMassRatio
        dataPost[index].df = - dataPointSG.ddf * config.chargeMassRatio / dV
        dataPost[index].I  =   dataPointSG.f

        # Find the places where the derivative change sign
        fSign = np.sign(dataPost[index].df)
        signChange = ((np.roll(fSign, 1) - fSign) != 0).astype(int)
        # As np.roll does a circular shift, we assume there is no change in first element
        signChange[0] = 0
        # Select when the derivative change sign
        signChangeTrue = np.where(signChange == 1)[0]
        # First peak is the firt sign change
        dataPost[index].indexPeak1 = signChangeTrue[0]
        # Second peak will be the maximum of the peak values minus the first peak
        secondPeakValues = dataPost[index].f[signChangeTrue[1:]]
        secondPeakIndex  = (np.where(secondPeakValues == max(secondPeakValues))[0] + 1)[0]
        dataPost[index].indexPeak2 = signChangeTrue[secondPeakIndex]
        # Identify where the minimum between peaks is
        fBetweenPeaks = dataPost[index].f[dataPost[index].indexPeak1:dataPost[index].indexPeak2]
        dataPost[index].indexSep = np.where(fBetweenPeaks == min(fBetweenPeaks))[0][0] + dataPost[index].indexPeak1

        # Calculate moments of the two integrals
        minIndex = 0
        maxIndex = dataPost[index].indexSep
        n1 = integrate.simps(y =                                                dataPost[index].f[minIndex:maxIndex], dx = dV)
        m1 = integrate.simps(y = sqrtList(dataPost[index].V[minIndex:maxIndex])*dataPost[index].f[minIndex:maxIndex], dx = dV)
        e1 = integrate.simps(y =          dataPost[index].V[minIndex:maxIndex] *dataPost[index].f[minIndex:maxIndex], dx = dV)
        minIndex = dataPost[index].indexSep + 1
        maxIndex = -1
        n2 = integrate.simps(y =                                                dataPost[index].f[minIndex:maxIndex], dx = dV)
        m2 = integrate.simps(y = sqrtList(dataPost[index].V[minIndex:maxIndex])*dataPost[index].f[minIndex:maxIndex], dx = dV)
        e2 = integrate.simps(y =          dataPost[index].V[minIndex:maxIndex] *dataPost[index].f[minIndex:maxIndex], dx = dV)

        dataPost[index].u1 = m1 / n1 * sqrt(config.chargeMassRatio)
        dataPost[index].T1 = e1 / n1 - (m1 / n1)**2
        dataPost[index].u2 = m2 / n2 * sqrt(config.chargeMassRatio)
        dataPost[index].T2 = e2 / n2 - (m2 / n2)**2

    return dataPost

def power(myList, int):
    return [x**int for x in myList]

def sqrtList(myList):
    return [sqrt(x) for x in myList]

def readRPAFile(filePath):
    fileObj = open(filePath)
    lines = fileObj.read().splitlines()
    fileObj.close()

    fileData = []
    # Read the lines avoiding the first one (header)
    for l in lines[1:]:
        columns = l.split()
        fileData.append(rawData(float(columns[1]), float(columns[2])))

    # Returns the data
    return fileData

def writeASCII(config, data):
    # Create main folder for the data
    createFolder(config.outputFolder)

    # file for peaks
    fileName = 'peaks.txt'
    peaks = open(config.outputFolder + fileName, 'w')
    header = '#{:>14}{:>15}{:>15}{:>15}{:>15}{:>15}{:>15}\n'.format('Z','X','Y',\
                                                                    'V Peak 1 (V)','f Peak 1 (a.u)', \
                                                                    'V Peak 2 (V)','f Peak 2 (a.u)')
    peaks.write(header)

    # file for momentum
    fileName = 'mome.txt'
    mome = open(config.outputFolder + fileName, 'w')
    header = '#{:>19}{:>20}{:>20}{:>20}{:>20}{:>20}{:>20}\n'.format('Z','X','Y',\
                                                                    'u Peak 1 (m s^-1)','T Peak 1 (eV)', \
                                                                    'u Peak 2 (m s^-1)','T Peak 2 (eV)')
    mome.write(header)

    # Create folder for distribution functions
    outputFolderDistr = config.outputFolder + 'distributionFunction/'
    createFolder(outputFolderDistr)

    for d in data:
        fileName = 'f_' + 'Z' + str(d.Z) + '_X' + str(d.X) + '_Y' + str(d.Y) + '.txt' 
        f = open(outputFolderDistr + fileName, 'w')
        header  = '# Position: Z={:8.3f} X={:8.3f} Y={:8.3f}\n'.format(d.Z, d.X, d.Y)
        header += '#{:>14}{:>15}{:>15}{:>15}\n'.format('Voltage (V)','f (a.u.)','df (a.u.)','Current (mA)')
        f.write(header)
        for i in range(len(d.V)):
            string = '{:15.3f}{:15.3e}{:15.3e}{:15.3e}\n'.format(d.V[i], d.f[i], d.df[i], d.I[i])
            f.write(string)
        f.close()

        V_peak1 = d.V[d.indexPeak1]
        f_peak1 = d.f[d.indexPeak1]
        V_peak2 = d.V[d.indexPeak2]
        f_peak2 = d.f[d.indexPeak2]
        string = '{:15.3f}{:15.3f}{:15.3f}{:15.3f}{:15.3e}{:15.3f}{:15.3e}\n'.format(d.Z, d.X, d.Y,    \
                                                                                     V_peak1, f_peak1, \
                                                                                     V_peak2, f_peak2)
        peaks.write(string)

        string = '{:20.3f}{:20.3f}{:20.3f}{:20.3e}{:20.3e}{:20.3e}{:20.3e}\n'.format(d.Z, d.X, d.Y, \
                                                                                     d.u1, d.T1,    \
                                                                                     d.u2, d.T2)   
        mome.write(string)
    
    peaks.close()

def plotFigures(config, data):

    if config.plotFigures:
        x = [d.Z for d in data]
        y = [d.X for d in data]
        z = [d.Y for d in data]

        # Peak of first distribution function
        c = [d.V[d.indexPeak1] for d in data]
        plot4Ddata(x, y, z, c, 'Peak 1 (eV)', 0, 100)
        plt.savefig(config.outputFolder + 'peak_1.png')

        # Velocity of first distribution function
        c = [d.u1/1000.0 for d in data]
        plot4Ddata(x, y, z, c, 'Velocity 1 (km s^-1)', 0, 20)
        plt.savefig(config.outputFolder + 'velo_1.png')

        # Temperature of first distribution function
        c = [d.T1 for d in data]
        plot4Ddata(x, y, z, c, 'Temperature 1 (eV)', 0, 10)
        plt.savefig(config.outputFolder + 'temp_1.png')

        # Peak of first distribution function
        c = [d.V[d.indexPeak2] for d in data]
        plot4Ddata(x, y, z, c, 'Peak 2 (eV)', 100, 400)
        plt.savefig(config.outputFolder + 'peak_2.png')

        # Velocity of second distribution function
        c = [d.u2/1000.0 for d in data]
        plot4Ddata(x, y, z, c, 'Velocity 2 (km s^-1)', 20, 40)
        plt.savefig(config.outputFolder + 'velo_2.png')

        # Temperature of second distribution function
        c = [d.T2 for d in data]
        plot4Ddata(x, y, z, c, 'Temperature 2 (eV)', 0, 10)
        plt.savefig(config.outputFolder + 'temp_2.png')

def plot4Ddata(x, y, z, c, value, cmin, cmax):
    fig = plt.figure(figsize = (8,6), dpi = 100)
    ax = fig.add_subplot(111, projection='3d')
    img = ax.scatter(x, y, z, c = c, cmap = plt.get_cmap('turbo'), vmin = cmin, vmax = cmax)
    cbar = fig.colorbar(img, shrink = 0.3, aspect = 8, pad = 0.08)
    cbar.set_label(value)
    ax.set_xlabel('Z (mm)')
    ax.set_ylabel('X (mm)')
    ax.set_zlabel('Y (mm)')
    fig.tight_layout()

def plotDistFunc(config, data):
    if config.plotDistFunc:
        for d in data:
            x  = d.V
            y  = d.f
            y2 = d.I

            fig, ax1 = plt.subplots(1, 1, figsize = (8,6), dpi = 100)
            ax2 = ax1.twinx()
            fileName = 'f_' + 'Z' + str(d.Z) + '_X' + str(d.X) + '_Y' + str(d.Y)
            plt.xlabel("Voltage (V)")
            ax1.set_ylabel("Distribution Function (a.u.)")
            ax2.set_ylabel("Fitted current (mA)")
            ax1.plot(x, y)
            ax2.plot(x, y2, color='red')
            plt.title(fileName)
            fig.tight_layout()
            plt.savefig(config.outputFolder + 'distributionFunction/' + fileName + '.png' )
            plt.close()


def createFolder(path):
    if not os.path.exists(path):
        os.makedirs(path)

if __name__ == "__main__":
    main()

